function [G,n,ntilde] = diagonal_core_generation(gamma,theta)
% DIAGONAL_CORE_GENERATION  construct a core G for (gamma,theta) based on 
% fixed permutations
%
%   [G,n,ntilde] = DIAGONAL_CORE_GENERATION(gamma,theta) expects a pair 
%   (gamma,theta) and returns a core G of length n for which
%       Gamma*G is left orthogonal and G*Theta is right-orthogonal
%   of size(G) = [deg(gamma),deg(theta),n], n = max(deg(gamma),deg(theta)).
%   The algorithm will not determine if (gamma,theta) is feasible for less
%   for n, but if all G(:,:,i) equal 0 for i > ntilde, then the pair is
%   feasible for ntilde.
%
%   The output core G will be sparsely populated.
%
%   Example:
%         gamma = sqrt([10,2,1,0,0]); 
%         theta = sqrt([4,3,2.5,2,1.5]);
%         [G,n,ntilde] = diagonal_core_generation(gamma,theta);
%         G
%
%   See also:
%       TEST_DIAGONAL_CORE_GENERATION

% reduce to positive part
gamma_plus = sort(gamma(gamma>0),'descend');
theta_plus = sort(theta(theta>0),'descend');

lg = length(gamma_plus);
lt = length(theta_plus);
n = max(lg,lt);

gamma_sq = [gamma_plus(:).^2',zeros(1,n-lg)];
theta_sq = [theta_plus(:).^2',zeros(1,n-lt)];

% init matrix of diagonal entries
A_ = zeros(n,n);
A_(:,1) = theta_sq;

% single permutation
pi = [2:n,1];

% iterate:
for j = 1:n-1
    for i = 1:n
        if sum(A_(i,:)) > gamma_sq(i)
            val = min(A_(i,j),sum(A_(i,:))-gamma_sq(i));
            A_(pi(i),j+1) = val;
            A_(i,j) = A_(i,j) - val;
        end
    end
end

% permutation matrix
PI = repmat((1:n)',[1,n]) + repmat(0:n-1,[n,1]);
PI = mod(PI-1,n) + 1;

% init:
A = cell(1,n);
B = cell(1,n);

r = min(lg,lt);
Gamma = diag(gamma_plus);
Theta = diag(theta_plus);

[V,S,U] = deal(cell(1,n));
[H,G] = deal(zeros(lg,lt,n));

for i = 1:n
    % diagonal matrices of permuted diagonal values:
    A{i} = diag(A_(PI(:,i),i));
    A{i} = A{i}(1:lt,1:lt); % restrict to neccessary size
    B{i} = diag(A_(:,i));
    B{i} = B{i}(1:lg,1:lg); % restrict to neccessary size
    
    % V{i} and U{i} will just be permutation matrices:
    [V{i},S{i},~] = svd(A{i},0);
    [U{i},S{i},~] = svd(B{i},0);

    S{i} = S{i}(1:r,1:r);
    V{i} = V{i}(:,1:r);
    U{i} = U{i}(:,1:r);
    
    % put together:
    H(:,:,i) = U{i}*S{i}.^(1/2)*V{i}';
    
    % substitute back:
    G(:,:,i) = Gamma\H(:,:,i)/Theta;
end

% test (the stability of the progress above could be improved)
% left orthogonality of Gamma*G:
GTG = 0;
for i = 1:n
    Gamma_Gi = Gamma*G(:,:,i);
    GTG = GTG + Gamma_Gi'*Gamma_Gi;
end

relerr = norm(GTG-eye(size(GTG)),'fro')/norm(eye(size(GTG)),'fro');
if relerr > 1e-10
    warning('Left-orthogonality constraint violated  (relerr = %.2e).',relerr);
end

% right orthogonality of G*Theta:
GGT = 0;
for i = 1:n
    Gi_Theta = G(:,:,i)*Theta;
    GGT = GGT + Gi_Theta*Gi_Theta';
end

relerr = norm(GGT-eye(size(GGT)),'fro')/norm(eye(size(GGT)),'fro');
if relerr > 1e-10
    warning('Right-orthogonality constraint violated (relerr = %.2e).',relerr);
end

ntilde = sum(sum(sum(abs(G),1),2)>0);
if ntilde < n
    fprintf('The pair (gamma,theta) is in fact feasible for ntilde = %d (or even less).\n',ntilde);
else
    fprintf('The pair (gamma,theta) is feasible for n = %d (or less).\n',n);
end






