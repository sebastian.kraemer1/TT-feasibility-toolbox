% function test_construct_tensor_with_sv()
% TEST_CONSTRUCT_TENSOR_WITH_SV  test function for construct_tensor_with_sv
%
%   TEST_CONSTRUCT_TENSOR_WITH_SV generates TT-singular values randomly and
%   runs construct_tensor_with_sv
%
%   Options are hardcoded.
%
%   See also:
%       CONSTRUCT_TENSOR_WITH_SV, ALTERNATING_CORE_GENERATION

% Random seed
rng(8)

% Order of tensor
d = 10;

% Minimal mode size
n = ones(1,d);

% Maximal rank
r_max = 30;

% Randomized rank
r = ceil([1,1+rand(1,d-1)*r_max,1]);
fprintf('Rank \nr = ');
fprintf('%3d ',r);
fprintf('\n');

% Somehow randomized sv
sigma = cell(1,d+1);

% Increasing alpha should lead to more "difficult" singular values
alpha = 10;

hold off
for mu = 1:d+1
    sigma{mu} = exp(-alpha*rand(1)*rand(1,r(mu)));
%     sigma{mu} = sigma{mu}.^p;
    sigma{mu} = sort(sigma{mu}/norm(sigma{mu}),'descend');
    semilogy(1:r(mu),sigma{mu},'b-o'); hold on
end
drawnow

% Call
[G,n_plus] = construct_tensor_with_sv(n,sigma);
