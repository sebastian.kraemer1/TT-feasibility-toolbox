function [G,var_error,success] = alternating_core_generation(n,gamma,theta,opts)
% ALTERNATING_CORE_GENERATION  checks if (gamma,theta) is feasible for n
%   and constructs an associated core G
%
%   ALTERNATING_CORE_GENERATION(n,gamma,theta) takes a natural number n
%   and a pair (gamma,theta) and checks numerically if it is feasible for n
%   and if so, returns a core G of length n for which
%       Gamma*G is left orthogonal and G*Theta is right-orthogonal
%
%   gamma and theta do not have to be of same length and may include zeros
%   both will be sorted in descending order
%
%   ALTERNATING_CORE_GENERATION(n,gamma,theta,opts) allows to specify
%   options regarding printing and tolerance
%
%   Example:
%       gamma = sqrt([10 2 1 0 0])
%       theta = sqrt([4 3 2.5000 2 1.5000])
%       alternating_core_generation(2,gamma,theta); % fails
%       fprintf('\n');
%       alternating_core_generation(3,gamma,theta); % fails
%       fprintf('\n');
%       alternating_core_generation(4,gamma,theta); % succeeds
%       fprintf('\n');
%
%   See also;
%       TEST_ALTERNATING_CORE_GENERATION, LINEAR_PROGRAMMING_CHECK

if nargin < 4
    opts = struct;
    opts.print = 1;
    opts.tol = 1e-14;
end

% trace property:
if abs(sum(gamma.^2) - sum(theta.^2)) > opts.tol
    warning('Trace property is violated! \n');
end

% init:
success = 0;
iter_max = 1000;
trial =0;
var_error = 1;

A = cell(1,n);
B = cell(1,n);

% (gamma,theta):
gamma = sort(gamma(gamma>0),'descend'); gamma = gamma(:)';
theta = sort(theta(theta>0),'descend'); theta = theta(:)';

% diagonal matrices:
Theta = diag(theta);
Gamma = diag(gamma);

% sizes
r1 = size(Gamma,1);
r2 = size(Theta,1);

if r1 > r2*n || r2 > r1*n
    if opts.print
        fprintf('Ranks make feasibility impossible! \n');
    end
    G = 0;
    var_error = inf;
    return
end

% main:
if opts.print
    fprintf('iter: ');
end
while var_error > opts.tol && trial <= 2
    trial = trial + 1;
    
    % random initialization
    rng(trial)
    
    % "H(i) = H(:,:,i)"
    H = randn(r1,r2,n);
    
    % iterate:
    var_error = Inf;
    i = 0;
    if opts.print
        fprintf('%6d',i)
    end
    
    % stop if tolerance achieved (success) or stagnates/max iter (no success)
    while i <= iter_max && var_error > opts.tol && ...
            ~( i > 10 && abs(var_error_old-var_error)/abs(var_error_old) < 1e-5 )
        
        if opts.print
            fprintf('\b\b\b\b\b\b%6d',i)
        end
        i = i + 1;
        
        % set theta
        [U,Theta_k,~] = left_SVD_(H); % get SVD of \mathfrak{L}(G)
        H = left_ASSIGN_(U*Theta); % \mathfrak{L}(G) <- U*Theta
        theta_k = diag(Theta_k)';
        
        % set gamma
        [~,Gamma_k,Vt] = right_SVD_(H); % get SVD of \mathfrak{R}(G)
        H = right_ASSIGN_(Gamma*Vt'); % \mathfrak{R}(G) <- Gamma*Vt'
        gamma_k = diag(Gamma_k)';
        
        % pointwise relative error
        var_error_old = var_error;
        var_error = max(max(abs(theta_k./theta-1)),max(abs(gamma_k./gamma-1)));
        
    end
    
    % manual summation (just for clearity):
    % theta:
    A_ = 0;
    for i = 1:n
        A{i} = H(:,:,i)'*H(:,:,i);
        A_ = A_ + A{i};
    end
    
    % gamma:
    B_ = 0;
    for i = 1:n
        B{i} = H(:,:,i)*H(:,:,i)';
        B_ = B_ + B{i};
    end
end
if opts.print
    fprintf('\n');
end

% from H to G:
G = randn(r1,r2,n);
r = min(r1,r2);

if norm(A_ - diag(diag(A_)),'fro')/norm(diag(A_)) < opts.tol && norm(B_ - diag(diag(B_)),'fro')/norm(diag(B_)) < opts.tol && issorted(-diag(A_)) && issorted(-diag(B_))
    % Short construction of G (should be the default)):
    for i = 1:n
        G(:,:,i) = Gamma\H(:,:,i)/Theta;
    end
else
    % Construction as in reduction-to-eigenvalues theorem (failsafe, but less stable):
    % (svds automatically sort the eigenvalues...)
    
    % diagonalize righthand side:
    [QA,~,~] = svd(A_,0);
    [QB,~,~] = svd(B_,0);
    
    [V,S,U] = deal(cell(1,n));
    for i = 1:n
        % decompose summands:
        A{i} = QA'*A{i}*QA;
        B{i} = QB'*B{i}*QB;
        
        [V{i},S{i},~] = svd(A{i},0);
        [U{i},S{i},~] = svd(B{i},0);
        
        S{i} = S{i}(1:r,1:r);
        V{i} = V{i}(:,1:r);
        U{i} = U{i}(:,1:r);
        
        % put together:
        H(:,:,i) = U{i}*S{i}.^(1/2)*V{i}';
        
        % substitute back:
        G(:,:,i) = Gamma\H(:,:,i)/Theta;
    end
end

if var_error < opts.tol
    success = true; % only line where success is set to true
    
    % left orthogonality of Gamma*G:
    GTG = 0;
    for i = 1:n
        Gamma_Gi = Gamma*G(:,:,i);
        GTG = GTG + Gamma_Gi'*Gamma_Gi;
    end
    
    relerr = norm(GTG-eye(size(GTG)),'fro')/norm(eye(size(GTG)),'fro');
    if relerr > 1e-12
        warning('Left-orthogonality constraint violated  (relerr = %.2e).',relerr);
    end
    
    % right orthogonality of G*Theta:
    GGT = 0;
    for i = 1:n
        Gi_Theta = G(:,:,i)*Theta;
        GGT = GGT + Gi_Theta*Gi_Theta';
    end
    
    relerr = norm(GGT-eye(size(GGT)),'fro')/norm(eye(size(GGT)),'fro');
    if relerr > 1e-12
        warning('Right-orthogonality constraint violated (relerr = %.2e).',relerr);
    end
    
    % if everything is fine as expteced:
    if opts.print
        fprintf('(gamma,theta) is (numerically) feasible for n = %d \n',n);
    end
else
    if opts.print
        fprintf('(gamma,theta) is likely to be not feasible for n = %d \n',n);
        %         fprintf('Yet, the output G is an approximation to the desired core. \n');
        %         fprintf('It does fulfill the right-orthogonality property. \n');
    end
end

% subfunctions:

    function H = left_ASSIGN_(U) % \mathfrak{L}(G) <- \mathfrak{L}(U)
        H = permute(reshape(U,[r1,n,r2]),[1,3,2]);
    end

    function H = right_ASSIGN_(Vt) % \mathfrak{R}(G) <- \mathfrak{R}(Vt)
        H = reshape(Vt,[r1,r2,n]);
    end

    function [U,Theta,V] = left_SVD_(H) % get SVD of \mathfrak{L}(G)
        [U,Theta,V] = svd(reshape(permute(H,[1,3,2]),[r1*n,r2]),'econ');
    end

    function [U,Theta,V] = right_SVD_(H) % get SVD of \mathfrak{R}(G)
        [U,Theta,V] = svd(reshape(H,[r1,r2*n]),'econ');
    end
end
