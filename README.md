--- --- --- 

For software copyright issues, see the LICENSE file.

Please note the copyright and citation issues implied by the publication and preprint versions of the related paper
"A geometrical description of feasible singular values in the Tensor Train format" by Sebastian Kraemer.

--- --- ---

HOW TO GET STARTED:

It is best to get started with the included live script file and/or the compiled pdf file. 
It contains a short overview about the included algorithms.
These contain small documentations themselves, and are briefly commented throughout the code. 
With an exception to 'linear_programming_check', the codes are quite short anyway.
For each algorithm, there is also a 'test_' file which generates some simple input and then runs it.
