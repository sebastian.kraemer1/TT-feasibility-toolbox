function [G,n_plus] = construct_tensor_with_sv(n,sigma)
% CONSTRUCT_TENSOR_WITH_SV  constructs a tensor with prescribed TT-singular
% values in parallel
%
%   CONSTRUCT_TENSOR_WITH_SV(n,sigma) expects a starting mode size n with
%   n(mu) >= 1 for all mu and a cell sigma, containing TT-singular values,
%   s.t. length(sigma) = length(n) + 1, and returns a TT-representation G
%   with size(G{mu}) = [r(mu),r(mu)+1,n_plus(mu)] for the possible 
%   increased mode size n_plus and r(mu) = length(sigma{mu})
%
%   CONSTRUCT_TENSOR_WITH_SV(n,sigma,opts) specifies options for printing
%   and tol
%
%   Example (from the talk):
%       sigma = cell(1,5);
%       sigma{1} = sqrt(13);
%       sigma{2} = [3,2];
%       sigma{3} = [sqrt(10),sqrt(2),1];
%       sigma{4} = [2,sqrt(3),sqrt(2.5),sqrt(2),sqrt(1.5)];
%       sigma{5} = sqrt(13)
%       n = [1,1,1,1];
%       [G,n_plus] = construct_tensor_with_sv(n,sigma)
%       % returns n_plus = [2,2,4,5]
%
%   See also:
%       TEST_CONSTRUCT_TENSOR_WITH_SV, ALTERNATING_CORE_GENERATION

if ~isa(sigma,'cell') || length(n)+1 ~= length(sigma)
    error('sigma must be a cell of length length(n)+1');
end

% options:
if nargin < 3
    opts = struct;
    opts.print = 0;
    opts.tol = 1e-14;
end

% init:
d = length(sigma)-1;
G = cell(1,d);
 
% ensure positive, descending values
for mu = 1:d+1
    sigma{mu} = sort(sigma{mu}(sigma{mu}>0),'descend');
end

% avoid communication:
sigma_copy = sigma;
it_shall_print = opts.print;

% main:
n_plus = n - 1;

parfor mu = 1:d % this loop may be run in parallel
    success = 0;
   
    % call alternating_core_generation as long as it has no success
    while ~success && (n_plus(mu)+1 <= length(sigma{mu}) || n_plus(mu)+1 <= length(sigma_copy{mu+1}))
        if it_shall_print
            fprintf('mu: %3d, n(mu): %4d \n',mu,n_plus(mu));
        end
        
        % increase mode size (repeatedly if neccessary):
        n_plus(mu) = n_plus(mu) + 1;
        
        % test with given mode size:
        [G{mu},~,success] = alternating_core_generation(n_plus(mu),sigma{mu},sigma_copy{mu+1},opts);        
    end
    
    % no success despite maximal required mode size (should not happen):
    if ~success
        error('Reached maximal mode size for mu = %3d, but alt. core. gen. failed.\n',mu);           
    end
end

% print result:
fprintf('sigma is (numerically) feasible for \nn =   ');
fprintf('%3d ',n_plus);
fprintf('\n');

