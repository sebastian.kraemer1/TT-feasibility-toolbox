% function test_diagonal_core_generation()
% TEST_DIAGONAL_CORE_GENERATION  tests diagonal_core_generation
%
%   test diagonal_core_generation for a certain random pair (gamma,theta)
%
%   See also:
%       DIAGONAL_CORE_GENERATION

n = 10;
gamma = rand(1,n);
gamma(end-3:end) = 0;

gamma = gamma/norm(gamma);
theta = rand(1,n);
theta = theta/norm(theta);

G = diagonal_core_generation(gamma,theta);
s = size(G)