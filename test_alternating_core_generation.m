% function test_alternating_core_generation()
% TEST_ALTERNATING_CORE_GENERATION  tests alternating_core_generation
%
%   TEST_ALTERNATING_CORE_GENERATION() sets (gamma,theta) and tests
%   alternating_core_generation
%
%   See also:
%       ALTERNATING_CORE_GENERATION

gamma = sqrt([10 2 1 0.1 1e-20]);
theta = sqrt([4 3 2.5000 2 1.5000]);
gamma = gamma/norm(gamma);
theta = theta/norm(theta);

alternating_core_generation(2,gamma,theta); % fails
fprintf('\n');
alternating_core_generation(3,gamma,theta); % fails
fprintf('\n');
G = alternating_core_generation(4,gamma,theta); % succeeds
fprintf('\n');