% function test_linear_programming_check()
% TEST_LINEAR_PROGRAMMING_CHECK  various tests for linear_programming_check
%
%   TEST_LINEAR_PROGRAMMING_CHECK() generates some pair (gamma,theta) and
%   finds the smallest natural number n for which it is feasible. It plots
%   the hive which provides the feasibility.
%
%   Change mode within the code to test another example
%
%   Please note that this code uses honeycomb notation (not tensor
%   notation). n is the size of the hermitian matrices.
%
%   See also:
%       LINEAR_PROGRAMMING_CHECK

% choose example (see below):
mode = 6;

% random seed:
rng(1);

% drawing options for linear_programming_check
drawoptions = struct;
drawoptions.compact = 0; % 1: more compact plot, but harder to distinguish
drawoptions.single = 1; % 1: also plots the single honeycombs

switch mode
    case 0 % A very simple example (feasible for 3)
        gamma_sq = [3,0,0];
        theta_sq = [1,1,1];
        
    case 1 % This generates the plot for the first hive in the paper (feasible for 2)
        gamma_sq = [7.5,5,0,0];
        theta_sq = [6,3.5,2,1];
        
    case 2 % Uniform vs exponential decay
        alpha = 4;
        n = 10;
        theta_sq = rand(1,n);
        theta_sq = theta_sq/sum(theta_sq);
        gamma_sq = exp(-alpha*(1:n)).*rand(1,n);
        gamma_sq = gamma_sq/sum(gamma_sq);
      
    case 3 % Reverse engineered to be feasible for at most m_half
        n = 10;
        m_half = 5;

        H1 = 0; H2 = 0;
        for i = 1:m_half
            a = exp(-1.2*(1:n)).*rand(1,n);
            
            [Q,~] = qr(rand(n)-0.5);
            Ai = Q*diag(a)*Q';
            H1 = H1 + Ai;
            
            [Q,~] = qr(rand(n)-0.5);
            Bi = Q*diag(a)*Q';
            H2 = H2 + Bi;
        end
        gamma_sq = sort(eig(H1),'descend')';
        theta_sq = sort(eig(H2),'descend')' ;
        
    case 4 % A more "difficult" random initialization
        n = 5;
        gamma_sq = sort(rand(1,n),'descend').^4;
        theta_sq = sort(rand(1,n),'descend').^4;
        gamma_sq = gamma_sq/sum(gamma_sq);
        theta_sq = theta_sq/sum(theta_sq);
        
    case 5 % Feasible for n = 4
        gamma_sq = [4,0,0,0];
        theta_sq = [1,1,1,1];
        
    case 6 % This generates the plot for the second hive in the paper (feasible for 4)
        gamma_sq = [10,2,1,0,0]; 
        theta_sq = [4,3,2.5,2,1.5];
 
end

%% Run
gamma = sqrt(gamma_sq);
theta = sqrt(theta_sq);
linear_programming_check(gamma,theta,drawoptions);
