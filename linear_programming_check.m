function feasible_for = linear_programming_check(gamma,theta,drawoptions)
% LINEAR_PROGRAMMING_CHECK  investigate the feasibility of a pair (gamma,theta)
%
%   LINEAR_PROGRAMMING_CHECK(n,gamma,theta,drawoptions) finds the smallest
%   mode size m <= n for which (gamma,theta) is feasible and plots the hive
%   obtained through the linear programming algorithm.
%
%   gamma and theta must be arrays of length n but may contain zeros.
%
%   Please note that this code uses honeycomb notation (not tensor
%   notation). n is the size of the hermitian matrices.
%
%   Options :
%
%   drawoptions.compact == 1: less distance between honeycombs
%   drawoptions.single == 1: also draw single honeycombs
%
%   Example:
%        gamma = sqrt([10,2,1]); 
%        theta = sqrt([4,3,2.5,2,1.5]);
%        
%        drawoptions = struct;
%        drawoptions.compact = 0;
%        drawoptions.single = 1;
% 
%        linear_programming_check(gamma,theta,drawoptions)
%
% --- Hive numbering and coupling (m_half = 5):
% each number represents a honeycomb
%
%      c   e
%      |   |
%  a   2   4    gamma
%   \ / \ / \ /         upper part (gamma)
%    1   3   5
%    |   |   |
%    b   d   f
%
%      c   e
%      |   |
%  a   7   9    theta
%   \ / \ / \  /        lower part (theta)
%    6   8   10
%    |   |   |
%    b   d   f
%
% --- Orientation of honeycombs (numbering with respect to upper part)
%
% even number:
%
%  in(1) (>=0) \ / coupled(2) (>=0)
%               |
%              out(3) (<=0)
%
% odd number:
%
%  coupled(1) (>=0) \ / in(2) (>=0)
%                    |
%                   out(3) (<=0)
%
% first: in = coupled
% last: out = prescribed
%
% --- Honeycomb vertex numbering, leaving out intermediate ones (n = 3):
% each number represents a vertex
%
%     \1/
%      |
%   \2/ \3/
%    |   |
% \4/ \5/ \6/
%  |   |   |          Y(0,0,0) (zero coordinate point is here)
%
% --- Single vertex (Y) numering:
% each number represents a line
%
% 1   2
%  \ /
%   |
%   3
%
% in direction of:
% 1: (0,1,-1)
% 2: (-1,0,1)
% 3: (1,-1,0)
%
%   See also:
%      TEST_LINEAR_PROGRAMMING_CHECK, ALTERNATING_CORE_GENERATION

if nargin < 3
    drawoptions = struct;
    drawoptions.compact = 0;
    drawoptions.single = 1;
end

lg = length(gamma);
lt = length(theta);
n = max(lg,lt);
feasible_for = n;

gamma_sq = [gamma(:).^2',zeros(1,n-lg)];
theta_sq = [theta(:).^2',zeros(1,n-lt)];

% global markerscale;
markerscale = 1/4;

mrr = 1e-5; % add this value for the residual division (in order to not
% divide by zero)
rng('shuffle');

m_half_start = 1;

if max([gamma_sq,theta_sq])/min([gamma_sq,theta_sq]) > 1e7
    warning('The problems condition is bad!');
end
if abs(sum(theta_sq)-sum(gamma_sq)) > 1e-13
    error('Trace property violated');
end
if length(gamma_sq) ~= n || length(theta_sq) ~= n
    error('gamma and theta do not have same length');
end

figure(1); hold off;
gamma_sq = sort(gamma_sq,'descend');
theta_sq = sort(theta_sq,'descend');
gammaplot = semilogy(1:n,gamma_sq,'bo-'); hold all;
thetaplot = semilogy(1:n,theta_sq,'ro-');
legend([gammaplot,thetaplot],'\gamma_+^2','\theta_+^2');

exitflag = -2;
m_half = m_half_start;

%% References to vertices
lvl_map = @(m) ceil(-1/2+(1/2)*sqrt(1+8*m)); % lvl on which number lies
first_on_lvl_map = @(m) m.*(m-1)/2 + 1; % first number on lvl (left side)
last_on_lvl_map = @(m) (m+1).*m/2; % last number on lvl (right side)

num_Y = last_on_lvl_map(n); % number of vertices

% same as vectors:
lvl = lvl_map(1:num_Y)';
first_on_lvl = first_on_lvl_map(1:n)';
last_on_lvl = last_on_lvl_map(1:n)';
last_lvl = num_Y-(n-1):num_Y;

% first column of tri_force: vertex numbers with south
% second column of tri_force: vertex numbers with north east
% third column of tri_force: vertex numbers with north west
T = last_on_lvl(n-1);
tri_force = [(1:T)',(1:T)'+lvl((1:T)'),(1:T)'+lvl((1:T)')+1];

%% Structure matrices for single honeycombs
% Inequalities
ineq = zeros(3,T,2,  3,num_Y); % (###ineq, #line,#vertex)
% Inner equalities
eq0 = zeros(num_Y+(num_Y-n),  3,num_Y); % (#ineq, #line,#vertex)
% Outer equalities
eqv = zeros(n,3, 3,num_Y); % (##ineq, #line,#vertex)
% Edge length
edge_length = zeros(3,T,3,num_Y); % minimize to generate largest lift

%% Lower and upper bounds
% a_b, b_n >= 0, c_n <=0 :
lower_bound = zeros(3,num_Y);
upper_bound = zeros(3,num_Y);
lower_bound(1:2,1:num_Y) = 0;
upper_bound(1:2,1:num_Y) = max([gamma_sq,theta_sq]);
lower_bound(3,1:num_Y) = -max([gamma_sq,theta_sq]);
upper_bound(3,1:num_Y) = 0;

%% Inner equalities
for y = 1:num_Y
    eq0(y, :, y) = 1; % vertex lines sum to 0
end
for t = 1:T % intermediate vertex lines sum to 0
    eq0(num_Y + t, 3, tri_force(t,1)) = 1;
    eq0(num_Y + t, 2, tri_force(t,2)) = 1;
    eq0(num_Y + t, 1, tri_force(t,3)) = 1;
end

%% Outer equalities
for i = 1:n % pick entries for boundary values
    eqv(i,1,  1,first_on_lvl(i)) = 1; % north west (..<..)
    eqv(n-(i-1),2,  2,last_on_lvl(i)) = 1; % north east (..<..)
    eqv(i,3,  3,last_lvl(i)) = 1; % south (out) (..<.. (negative!))
end

%% Inequalities and total edge length
for t = 1:T % vertex lines do not switch sides (<=> positive edge length)
    ineq(1,t,1, 1,tri_force(t,1)) = 1;
    ineq(1,t,1, 1,tri_force(t,3)) = -1;
    
    ineq(1,t,2, 1,tri_force(t,3)) = 1;
    ineq(1,t,2, 1,tri_force(t,2)) = -1;
    
    edge_length(1,t,1,tri_force(t,1)) = -1;
    edge_length(1,t,1,tri_force(t,2)) = 1;
    
    ineq(2,t,1, 2,tri_force(t,1)) = -1;
    ineq(2,t,1, 2,tri_force(t,2)) = 1;
    
    ineq(2,t,2, 2,tri_force(t,2)) = -1;
    ineq(2,t,2, 2,tri_force(t,3)) = 1;
    
    edge_length(2,t, 2,tri_force(t,3)) = -1;
    edge_length(2,t, 2,tri_force(t,1)) = 1;
    
    ineq(3,t,1, 3,tri_force(t,2)) = 1;
    ineq(3,t,1, 3,tri_force(t,1)) = -1;
    
    ineq(3,t,2, 3,tri_force(t,1)) = 1;
    ineq(3,t,2, 3,tri_force(t,3)) = -1;
    
    edge_length(3,t, 3,tri_force(t,2)) = -1;
    edge_length(3,t, 3,tri_force(t,3)) = 1;
end

%% --------------- Trying different numbers of honeycombs -----------------
fprintf('trying: %3d',0);
while exitflag ~= 1 && m_half <= n-1
    fprintf('\b\b\b%3d',m_half+1);
    
    m = 2*m_half; % number of honeycombs
    e = {gamma_sq,theta_sq};
    
    %% Couplings and boundary conditions
    hive_fix = {[m/2,3],[m,3]}; % boundary values (gamma and theta)
    hive_coup_tau = cell(m/2-1,2);
    hive_coup_bc = cell(1,m/2);
    
    for i = 1:m/2-1 % connections within one part
        if mod(i,2) == 0
            hive_coup_tau{i,1} = [i,3;i+1,1]; % out to in
            hive_coup_tau{i,2} = [i+m/2,3;i+1+m/2,1];
        else
            hive_coup_tau{i,1} = [i,3;i+1,2]; %
            hive_coup_tau{i,2} = [i+m/2,3;i+1+m/2,2];
        end
    end
    for i = 1:m/2 % conncetion between upper and lower part of hive
        if mod(i,2) == 0
            hive_coup_bc{i} = [i,1; i+m/2,1];
        else
            hive_coup_bc{i} = [i,2; i+m/2,2];
        end
    end
    
    % list all
    hive_coup = [reshape(hive_coup_tau,[1,2*(m/2-1)]), hive_coup_bc];
    % add connection of first side of first honeycomb
    hive_coup{2*(m/2-1) + m/2 +1} = [1,1;m/2+1,1];
    
    %% Line inequalities in hive (each honeycomb gets a copy)
    % A:
    % (third component is redundant)
    INEQ = reshape(ineq(1:2,:,:,:),[2*T*2, 3*num_Y]);
    A = kron(eye(m),INEQ);
    
    % b:
    b = zeros(size(A,1),1); % inequalities do not contain constants
    
    %% edge length > 0 (as alternative)
    % A:
    % constraints = [-reshape(edge_length,[3*T,3*num_Y]);reshape(lower_bound,[2,3*num_Y])];
    % A = [blkdiag(constraints,constraints); [reshape(order,[2*(n-1),3*num_Y]), zeros(2*(n-1),3*num_Y)]];
    
    % b:
    % b = zeros(2 * (3*T + 2) + 2*(n-1),1);
    
    %% Upper and lower bounds (each honeycomb gets a copy)
    ub = repmat(reshape(upper_bound,[3*num_Y,1]),[m,1]);
    lb = repmat(reshape(lower_bound,[3*num_Y,1]),[m,1]);
    
    %% Line equalities (each honeycomb gets a copy)
    % (parts of Aeq)
    % inner equalities:
    EQ0 = reshape(eq0,[num_Y+(num_Y-n),  3*num_Y]);
    EQ0m = kron(eye(m),EQ0);
    
    % outer equalities:
    % boundary conditions (prescribed values):
    Hfix = zeros(n*length(hive_fix),m*3*num_Y);
    vfix = zeros(n*length(hive_fix),1);
    for i = 1:length(hive_fix)
        % lefthand: select honeycomb range (=) righthand:
        Hfix(n*(i-1) + (1:n),3*num_Y*(hive_fix{i}(1)-1) + (1:3*num_Y)) = reshape(eqv(:,hive_fix{i}(2),:,:),[n,3*num_Y]);
        if ismember(hive_fix{i},[1,2]) % (positive) boundary values are entered in decreasing order (..>..)
            v = fliplr(e{i});
        else
            v = -e{i};
        end
        vfix(n*(i-1) + (1:n)) = v; % set boundary condition
    end
    
    % coupled boundaries:
    Hcoup = zeros(n*length(hive_coup),m*3*num_Y);
    for i = 1:length(hive_coup)
        for k = 1:2
            W = (-1)^k*reshape(eqv(:,hive_coup{i}(k,2),:,:),[n,3*num_Y]);
            if hive_coup{i}(k,2) == 3
                W = -flipud(W); % set to standard orientation
            end
            Hcoup(n*(i-1) + (1:n),3*num_Y*(hive_coup{i}(k,1)-1) + (1:3*num_Y)) = W; % select honeycomb
        end
    end
    
    % put together:
    Aeq = [EQ0m; Hcoup; Hfix];
    
    % beq:
    beq = [zeros(size(EQ0m,1)+size(Hcoup,1),1); vfix];
    
    %% Ensure minimization of relative residual (for stability)
    Aeq(size(EQ0m,1)+size(Hcoup,1)+1:end,:) = diag(1./(vfix+mrr))*Aeq(size(EQ0m,1)+size(Hcoup,1)+1:end,:);
    beq(size(EQ0m,1)+size(Hcoup,1)+1:end) = vfix./(vfix+mrr);
    
    % function to minimize in linear programming (total edge length):
    EDGE_LENGTH = reshape(edge_length,[3*T,3*num_Y]);
    EDGE_LENGTHm = kron(eye(m),EDGE_LENGTH);
    f = sum(EDGE_LENGTHm,1);
    
    % scale inequalities as well
    A = A/min([gamma_sq,theta_sq]+mrr);
    
    % run linear programming code
    options = optimoptions('linprog','Display','off','Algorithm','dual-simplex','TolFun',1e-3);
    [x,~,exitflag] = linprog(f,A,b,Aeq,beq,lb,ub,[],options);
    
    % unsuccessefull:
    if exitflag ~= 1
        m_half = m_half + 1;
    end
end

%% -------------------------- PRINT OUTPUT --------------------------------

fprintf('\n\ngamma = '); fprintf(' %d ',gamma_sq);
fprintf('\n\ntheta = '); fprintf(' %d ',theta_sq);
fprintf('\n\n');

if exitflag ~= -2
    fprintf('Verification: min(A*x-b) = %.2e, norm(Aeq*x-beq) = %.2e \n\n',max(A*x-b),norm(Aeq*x-beq));
    fprintf('Running direct verification: '); % alternating SVDs
    [~,var_error] = alternating_core_generation(m_half+1,sqrt(gamma_sq),sqrt(theta_sq));
    fprintf('\n');
    fprintf('Direct verification gave error: %.2e\n\n',var_error);
    
    if var_error < 1e-10
        fprintf('Solution found for a minimal of %d/%d pairs of matrices: \n',m_half+1,n);
        feasible_for = m_half+1;
    else
        fprintf('Heuristic check unsuccessful --- situation remains unclear...');
        feasible_for = n;
    end
else % error exit flag for linear programming
    fprintf('The problem seems inaccessible to verification \n');
    return;
end
fprintf('A/B_%d + ',1:(m_half + 1));
fprintf('\b\b\b = diag(theta)/diag(gamma) \n');
fprintf('\n');

%% ---------------------------- Draw hive ---------------------------------

for PLOT = 1:drawoptions.single+1
    plot_single = PLOT-1;
    hfig = figure(PLOT+1); clf; hold all;
    
    % default from cardinal directions to R^2 plane coordinates
    Q = [-1/sqrt(6),-1/sqrt(6),2/sqrt(6);
        -1/sqrt(2),1/sqrt(2),0;
        1/sqrt(3),1/sqrt(3),1/sqrt(3)];
    
    % even numbered honeycombs
    %
    %  1   2             1
    %   \ /    --->      |
    %    |              / \
    %    3             2   3
    %
    if plot_single
        Q1 = Q;
    else
        Q1 = Q*[0,0,1; 0,1,0; 1,0,0];
    end
    
    % odd numbered honeycombs
    %
    %  1   2          -1   -3
    %   \ /    --->     \ /
    %    |               |
    %    3               -2
    %
    if plot_single
        Q2 = Q;
    else
        Q2 = Q*[-1,0,0; 0,0,-1; 0,-1,0];
    end
    
    % shape result (vertex coordinates) back to
    Xm = reshape(x,[3,num_Y,m]); % (#line,#vertex,#honeycomb)
    
    scal = max([gamma_sq,theta_sq])/n;
    dist = scal/15;
    dist_letter = scal/30;
    
    % zero coordinates of honeycombs (as cardinal directions)
    ZERO = zeros(3,m/2);
    
    if ~plot_single
        %% Shift zero coordinates
        for h_ = 1:m/2-1
            u = 0;
            
            % (for signs, consider if positive values will move lines away or towards
            % neighbouring honeycomb in diagram, requiring)
            if mod(h_,2) == 1
                % ensure top lines are right (+scal) of preceeding zero coordinate (in diagram)
                u = max(u,max(-ZERO(2,h_) - Xm(1,last_on_lvl(1),h_+1+[0;m/2]) + scal)); % take zero coord.
                
                if drawoptions.compact~=1
                    % ensure zero is north east (+scal) of preceeding north west lines (in diagram)
                    u = max(u,max(Xm(1,last_lvl(1),h_+[0;m/2]) + ZERO(3,h_+1) + scal));
                end
            else
                % ensure bottom lines are right (+scal) of preceeding top lines (in diagram)
                u = max(u,max(Xm(1,last_lvl(1),h_+[0;m/2]) + Xm(2,first_on_lvl(1),h_+1+[0;m/2]) + scal));
                
                if drawoptions.compact~=1
                    % ensure north east lines south east (+scal) of preceeding zero (in diagram)
                    u = max(u,max(ZERO(2,h_) - Xm(3,last_lvl(1),h_+1+[0;m/2]) + scal));
                end
            end
            
            if mod(h_,2) == 1 % move next honeycomb in direction -(north east in honeycomb)
                Xm(1,:,h_+1+[0,m/2]) = Xm(1,:,h_+1+[0,m/2]) + u;
                Xm(3,:,h_+1+[0,m/2]) = Xm(3,:,h_+1+[0,m/2]) - u;
                ZERO(:,h_+1) = ZERO(:,h_+1) + [u,0,-u]'; % as well as its formal zero
                
                if h_+2 <= m/2 % move second next such that coordinates remain fitting (direction south in honeycomb)
                    Xm(1,:,h_+2+[0,m/2]) = Xm(1,:,h_+2+[0,m/2]) + u;
                    Xm(2,:,h_+2+[0,m/2]) = Xm(2,:,h_+2+[0,m/2]) - u;
                    ZERO(:,h_+2) = ZERO(:,h_+2) + [u,-u,0]'; % as well as its formal zero
                end
                
            else % move next honeycomb in direction -(north west in honeycomb)
                Xm(2,:,h_+1+[0,m/2]) = Xm(2,:,h_+1+[0,m/2]) - u;
                Xm(3,:,h_+1+[0,m/2]) = Xm(3,:,h_+1+[0,m/2]) + u;
                ZERO(:,h_+1) = ZERO(:,h_+1) + [0,-u,u]'; % as well as its formal zero
                
                if h_+2 <= m/2 % move second next such that coordinates remain fitting (direction south in honeycomb)
                    Xm(2,:,h_+2+[0,m/2]) = Xm(2,:,h_+2+[0,m/2]) - u;
                    Xm(1,:,h_+2+[0,m/2]) = Xm(1,:,h_+2+[0,m/2]) + u;
                    ZERO(:,h_+2) = ZERO(:,h_+2) + [u,-u,0]'; % as well as its formal zero
                end
                
            end
        end
    end
    
    LP = cell(m_half,1); % save all plotted text in this one
    ALL = cell(m_half,1); % save all points in this one
    for i = 1:m_half
        LP{i} = [];
        ALL{i} = [];
    end
    
    COL = {[0,84,159]/255,[227,0,102]/255};
    
    bound = struct;
    for h = 1:m/2+1
        bound.(char(96+h)) = zeros(1,n);
    end
    
    for h = 1:m
        if plot_single
            subplot(1,m/2,mod(h-1,m/2)+1); % figure(1+mod(h-1,m/2)+1);  plot([0,0],[0,0],'ko-'); hold all;
        end
        hold all;
        X = Xm(:,:,h);
        if mod(mod(h-1,m/2)+1,2) == 1
            Q = Q2;
        else
            Q = Q1;
        end
        
        % plot inner lines (those that contribute to "length")
        for t = 1:T
            % constant coordinates of lines running from three neighboured
            % vertices into one non-numbered vertex -> give coordinates of this
            % vertex -> give x/y coordinates by multiplication with Q
            p = Q*[X(1,tri_force(t,3));X(2,tri_force(t,2));X(3,tri_force(t,1))];
            
            % coordinates of neighbouring vertices
            P = Q*X(:,tri_force(t,:));
            
            % print lines to neighbouring vertices (the three connected to p)
            %
            %     \   /
            %     P(:,1)
            %       |
            %       p
            % \   /   \   /
            % P(:,2)  P(:,3)
            %   |       |
            %
            ALL{mod(h-1,m/2)+1} = [ALL{mod(h-1,m/2)+1},p,P];
            for i = 1:3
                plot([p(1)-dist*(h>m/2)/sqrt(3),P(1,i)-dist*(h>m/2)/sqrt(3)],[p(2)-dist*(h>m/2),P(2,i)-dist*(h>m/2)],'-','color',COL{(h>m/2)+1},'markersize',markerscale*20);
            end
        end
        
        % plot rays
        for i = 1:n
            
            % north west rays:
            if mod(mod(h-1,m/2)+1,2) == 0 || mod(h-1,m/2)+1 == 1 || plot_single % first or even numbered
                y = first_on_lvl(i);
                p = Q*X(:,y);
                
                % ensure blue and red ray end at the same point
                scal_ = max(0,Xm(2,y,mod(h + m/2 - 1,m) + 1)-Xm(2,y,h)) + scal;
                
                q = Q*(X(:,y) + scal_*[0;1;-1]);
                ALL{mod(h-1,m/2)+1} = [ALL{mod(h-1,m/2)+1},p];
                plot([p(1)-dist*(h>m/2)/sqrt(3),q(1)-dist*(h>m/2)/sqrt(3)],[p(2)-dist*(h>m/2),q(2)-dist*(h>m/2)],'k-*','color',COL{(h>m/2)+1},'markersize',markerscale*20);
                
                % plot letter (currently without prefix)
                if h <= m/2
                    if h == 1
                        % prae = '-';
                        prae = '';
                    else
                        prae = '';
                    end
                    if mod(mod(h-1,m/2)+1,2) == 0 || mod(h-1,m/2)+1 == 1 || ~plot_single
                        bound.(char(96+h+(h>1)))(:,n-(i-1)) = round(X(1,y)-ZERO(1,mod(h-1,m/2)+1),10);
                        LP = mark_with_letter(prae,char(96+h+(h>1)),q,LP,n-(i-1),dist_letter,1,X(1,y)-ZERO(1,mod(h-1,m/2)+1),mod(h-1,m/2)+1,plot_single);
                    end
                end
            end
            
            % north east rays:
            if mod(mod(h-1,m/2)+1,2) == 1 || plot_single % odd numbered
                y = last_on_lvl(i);
                p = Q*X(:,y);
                
                % ensure blue and red ray end at the same point
                scal_ = max(0,Xm(1,y,h)-Xm(1,y,mod(h + m/2 - 1,m) + 1)) + scal;
                q = Q*(X(:,y) + scal_*[-1;0;1]);
                ALL{mod(h-1,m/2)+1} = [ALL{mod(h-1,m/2)+1},p];
                plot([p(1)-dist*(h>m/2)/sqrt(3),q(1)-dist*(h>m/2)/sqrt(3)],[p(2)-dist*(h>m/2),q(2)-dist*(h>m/2)],'-*','color',COL{(h>m/2)+1},'markersize',markerscale*20);
                
                % plot letter
                if h <= m/2
                    if mod(mod(h-1,m/2)+1,2) == 1 || ~plot_single
                        bound.(char(96+h+1))(:,i) = round(X(2,y)-ZERO(2,mod(h-1,m/2)+1),10);
                        LP = mark_with_letter('',char(96+h+1),q,LP,i,dist_letter,-1,X(2,y)-ZERO(2,mod(h-1,m/2)+1),mod(h-1,m/2)+1,plot_single); % ('-',...
                    end
                end
            end
            
            % south rays:
            if mod(h-1,m/2)+1 == m/2 || plot_single % last one
                y = last_lvl(i);
                p = Q*X(:,y);
                
                q = Q*(X(:,y) + scal*[1;-1;0]);
                ALL{mod(h-1,m/2)+1} = [ALL{mod(h-1,m/2)+1},p];
                plot([p(1)-dist*(h>m/2)/sqrt(3),q(1)-dist*(h>m/2)/sqrt(3)],[p(2)-dist*(h>m/2),q(2)-dist*(h>m/2)],'-*','color',COL{(h>m/2)+1},'markersize',markerscale*20);
                
                % plot letter
                if h <= m/2
                    LETTER_ = '\gamma^2';
                else
                    LETTER_ = '\theta^2';
                end
                if ~plot_single || mod(h-1,m/2)+1 == m/2
                    bound.(strrep(LETTER_(2:end),'^2','_sq'))(:,i) = -round(X(3,y)-ZERO(3,mod(h-1,m/2)+1),10);
                    LP = mark_with_letter('-',LETTER_,q,LP,i,dist_letter,-1,X(3,y)-ZERO(3,mod(h-1,m/2)+1),mod(h-1,m/2)+1,plot_single);
                end
            end
            
        end
        
    end
    
    % plot zero coordinates of coupled honeycombs
    for h = 1:m/2
        if plot_single
            subplot(1,m/2,mod(h-1,m/2)+1); % figure(1+mod(h-1,m/2)+1); 
        end
        if mod(h,2) == 1
            Q = Q2;
        else
            Q = Q1;
        end
        dir = [[0;1;-1],[-1;0;1],[1;-1;0]];
        
        for i = 1:3
            p = Q*(ZERO(:,h) + 1/2*scal*dir(:,i));
            q = Q*ZERO(:,h);
            plot([p(1)-dist/4,q(1)-dist/4],[p(2)-dist/2,q(2)-dist/2],'k--');
        end
        p = Q*ZERO(:,h); % + scal*dir(:,i));
        plot([p(1)-dist/4,p(1)-dist/4],[p(2)-dist/2,p(2)-dist/2],'ko','markersize',markerscale*50,'linewidth',markerscale*2)
    end
    
    % draw connections between consecutive honeycombs
    if ~plot_single
        for h_ = [1:m/2-1,m/2+1:m-1]
            for i = 1:n
                if mod(mod(h_-1,m/2)+1,2) == 1
                    q = Q2*Xm(:,last_lvl(i),h_);
                    p = Q1*Xm(:,last_on_lvl(i),h_+1);
                else
                    q = Q1*Xm(:,last_lvl(i),h_);
                    p = Q2*Xm(:,first_on_lvl(n-(i-1)),h_+1);
                end
                plot([p(1)-dist*(h_>m/2)/sqrt(3),q(1)-dist*(h_>m/2)/sqrt(3)],[p(2)-dist*(h_>m/2),q(2)-dist*(h_>m/2)],'k-','color',COL{(h_>m/2)+1},'markersize',markerscale*20,'linewidth',markerscale*2)
            end
        end
    end
      
    % scale window(s) appropriately
    if plot_single
        for h = 1:m/2
            subplot(1,m/2,mod(h-1,m/2)+1); % figure(1+mod(h-1,m/2)+1); 
            set(gca,'Visible','off')
            % set(gca,'xtick',[],'ytick',[])
            xmin = min(ALL{h}(1,:))-scal-1/2*(1-mod(h,2))*scal;
            xmax = max(ALL{h}(1,:))+scal+1/2*mod(h,2)*scal;
            ymin = min(ALL{h}(2,:))-scal;
            ymax = max(ALL{h}(2,:))+scal;
            axis([xmin xmax ymin ymax]);
            dx = xmax-xmin;
            dy = ymax-ymin;
            d_ = max(dx,dy);
            
            width = 3*400*(xmax-xmin)/d_;
            height = 3*400*(ymax-ymin)/d_;
            if width > 2000
                height = height/width*2000;
                width = 2000;
            end
        end
        set(hfig, 'Position', [1500 1000 width height/m_half])
    else
        xmin = Inf; xmax = -Inf;
        ymin = Inf; ymax = -Inf;
        for h = 1:m/2
            xmin = min(xmin,min(ALL{h}(1,:))-3*scal);
            xmax = max(xmax,max(ALL{h}(1,:))+3*scal);
            ymin = min(ymin,min(ALL{h}(2,:))-3*scal);
            ymax = max(ymax,max(ALL{h}(2,:))+3*scal);
        end
        axis([xmin xmax ymin ymax]);
        dx = xmax-xmin;
        dy = ymax-ymin;
        d_ = max(dx,dy);
        
        width = 3*400*(xmax-xmin)/d_;
        height = 3*400*(ymax-ymin)/d_;
        if width > 2000
            height = height/width*2000;
            width = 2000;
        end
        
        set(hfig, 'Position', [1500 1000 width height])
        set(gca,'Visible','off')
        % set(gca,'xtick',[],'ytick',[])
    end
    
    
end

fprintf('Boundary values (gamma, theta are squared here): \n\n')
disp(bound)

end
%% --- end main ---


function LP = mark_with_letter(praefix,LETTER,q,LP,i,dist,ori,value,plotnr,plot_single)
% plots a letter at the nearest, free location

dist_ = 1;
if plot_single
    range = plotnr;
else
    range = 1:size(LP,1);
end

% moves letter upwards until distance to other letters is large enough
while dist_ > 1e-4
    dist2 = Inf;
    for h = range
        for j = 1:size(LP{h},2)
            dist2 = min(dist2,norm([1;1;0].*(q - LP{h}(:,j))));
        end
    end
    dist_ = max(0,10*dist - dist2);
    q(2) = q(2) + ori*dist_;
end

% text(q(1),q(2),sprintf('%s%s_{%d}=%1.1f',praefix,LETTER,i,value),'HorizontalAlignment','left','fontsize',11);
text(q(1),q(2),sprintf('%s%s_{%d}',praefix,LETTER,i),'HorizontalAlignment','left','fontsize',9);
LP{plotnr} = [LP{plotnr},q]; % saves position at which letter was plotted
end







